#ifndef NETWORK_EXPR_HTTP_EXCEPTION_HPP
#define NETWORK_EXPR_HTTP_EXCEPTION_HPP
#include <stdexcept>
namespace lzhlib
{
    struct http_exception : std::runtime_error
    {
        http_exception(std::string const &s, std::string_view error_code)
            : runtime_error(s), error_code(error_code)
        {

        }
        std::string_view error_code;
    };
    struct http_bad_request : http_exception
    {
        explicit http_bad_request(std::string const &s)
            : http_exception(s, "400")
        {}
    };
    struct http_not_found : http_exception
    {
        explicit http_not_found(std::string const &s)
            : http_exception(s, "404")
        {}
    };
    struct http_method_not_supported : http_exception
    {
        explicit http_method_not_supported(std::string const &s)
            : http_exception(s, "405")
        {}
    };
    struct http_service_unavailable : http_exception
    {
        explicit http_service_unavailable(std::string const &s)
            : http_exception(s, "503")
        {}
    };
}
#endif //NETWORK_EXPR_HTTP_EXCEPTION_HPP
