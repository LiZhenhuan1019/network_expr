#pragma once
#include <stdexcept>           // for std::runtime_error
#include <utility>             // for std::move, std::pair
#include <optional>            // for std::optional
#include <list>                // for std::list
#include <thread>              // for std::thread
#include <mutex>               // for std::mutex, std::lock_guard, std::unique_lock
#include <future>              // for std::future
#include <condition_variable>  // for std::condition_variable
#include <winsock2.h>
#include <cassert>             // for assert macro
#include "thread_pool.hpp"
#include "http_server_config.hpp"
#include "http_connection.hpp"
#include "logger.hpp"
namespace lzhlib
{
    struct server_start_error : std::runtime_error
    {
        explicit server_start_error(std::string const &s)
            : runtime_error(s)
        {}
    };
    struct server_wrong_version : std::runtime_error
    {
        explicit server_wrong_version(std::string const &s)
            : runtime_error(s)
        {}
    };

    class winsock2_api_handler
    {
        struct raii
        {
            WORD version = MAKEWORD(2, 2);
            WSADATA wsadata = {};
            raii()
            {
                int result = WSAStartup(version, &wsadata);
                if (result)
                    throw server_start_error("http server cannot start");
                if (wsadata.wVersion != version)
                    throw server_wrong_version("version of http server does not match");
            }
            ~raii()
            {
                WSACleanup();
            }
        };
        raii r;
    };
    template <typename LoggerT>
    class http_server
    {
        using logger_t = LoggerT;
    public:
        explicit http_server(http_server_config config, logger_t &logger)
            : config(std::move(config)), threads(config.max_connections), logger(logger)
        {
            start_helper();
        }
        void restart()
        {
            stop_wait();
            start_helper();
        }

        ~http_server()
        {
            stop();
        }
    private:
        http_server_config config;
        lzhlib::thread::thread_pool<void(void)> threads;
        std::list<std::pair<std::future<void>, socket_handler>> connections;
        std::mutex start_mutex;
        std::condition_variable start_contition;
        std::thread accept_thread;
        std::optional<lzhlib::thread::thread_joiner> joiner;
        std::optional<tcp_listen_socket> listen_socket;
        std::optional<socket_handler> listen_socket_handler;
        logger_t &logger;

        void start_helper()
        {
            accept_thread = std::thread{&http_server::accept_thread_function, this};
            joiner.emplace(accept_thread);
            {
                auto[handler, socket] = tcp_listen_socket::create(config);
                std::lock_guard<std::mutex> lock(start_mutex);
                listen_socket.emplace(socket);
                listen_socket_handler.emplace(std::move(handler));
            }
            start_contition.notify_one();
        }
        void stop()
        {
            std::lock_guard<std::mutex> lock(start_mutex);
            listen_socket_handler.reset();
        }
        void stop_wait()
        {
            stop();
            joiner.reset();
        }
        void accept_thread_function()
        {
            std::optional<tcp_listen_socket> accept_socket;
            {
                std::unique_lock<std::mutex> lock(start_mutex);
                start_contition.wait(lock, [this] { return listen_socket_handler.has_value(); });
                accept_socket.emplace(listen_socket.value());
            }
            while (run())
            {
                try
                {
                    auto[handler, tcp_communicate_socket] = accept_socket->accept();
                    http_connection connection(tcp_communicate_socket, config, logger);
                    connections.remove_if([](auto &pair)
                                          {
                                              return pair.first.wait_for(std::chrono::seconds(0)) == std::future_status::ready;
                                          });
                    if (connections.size() >= config.max_connections)
                        connection.service_unavailable();
                    else
                    {
                        auto &[future, h] = connections.emplace_back(std::future<void>{}, std::move(handler));
                        auto f = threads.submit(connection);
                        future = std::move(f);
                    }
                }
                catch (socket_accept_error const &e)
                {
                }
            }
        }
        bool run()
        {
            std::lock_guard<std::mutex> lock(start_mutex);
            return listen_socket_handler.has_value();
        }
    };
}