#include <iostream>
#include "../include/http_server_config_reader.hpp"
#include "../include/http_server.hpp"
int main()
{
    lzhlib::string_logger string_logger;
    lzhlib::thread_safe_logger<lzhlib::string_logger> logger(string_logger);
    string_logger.set_flush_handler([](lzhlib::string_logger &logger)
                                    {
                                        std::cout << logger.str();
                                        logger.clear();
                                    });
    lzhlib::winsock2_api_handler sock_api_handler;
    lzhlib::http_server_config_reader config;
    lzhlib::http_server server(config.read(), logger);
    std::string stop;
    while (std::cin >> stop)
    {
        if (stop == "stop")
        {
            break;
        }
    }

    return 0;
}